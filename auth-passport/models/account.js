var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
  local            : {
      username        : String,
      password     : String,
  },
  google           : {
      id           : String,
      token        : String,
      email        : String,
      name         : String
  }
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);
